import Head from "next/head";
import React from "react";

/**
 * This component is used to set the HTML head.
 * @returns {React.JSX.Element}
 * @constructor
 */
const HtmlHead: React.FC = (): React.JSX.Element => {
    return (
        <>
            <Head>
                <meta charSet={"UTF-8"} />
                <meta
                    httpEquiv={"X-Content-Security-Policy"}
                    content={"default-src 'self'; script-src 'self'"}
                />
                <title>Hello from Electron renderer!</title>
            </Head>
        </>
    );
};
export default HtmlHead;
