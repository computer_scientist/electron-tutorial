import React from "react";
import styles from "../styles/main.module.scss";

/**
 * This component is used to display the main content of the page.
 * @returns {React.JSX.Element}
 * @constructor
 */
const ContentHeader: React.FC = (): React.JSX.Element => {
    return (
        <header className={styles.heroHeadline}>
            <h1>Hello from Electron Renderer!</h1>
            <p>👏</p>
        </header>
    );
};
export default ContentHeader;
