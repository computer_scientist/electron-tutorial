import React from "react";
import Image from "next/image";
import styles from "../styles/main.module.scss";

const imagePaths: Map<string, string> = new Map<string, string>();
imagePaths.set("node", "logos/nodejs_logo.svg");
imagePaths.set("chromium", "logos/chromium_logo.svg");
imagePaths.set("electron", "logos/electron_logo.svg");

/**
 * This component is used to display the versions of Node.js, Chrome, and
 * Electron.
 * @returns {React.JSX.Element}
 * @constructor
 */
const DisplayVersions: React.FC = (): React.JSX.Element => {
    return (
        <div>
            {window && window.versions ? (
                <div className={styles.versionBlock}>
                    <div className={styles.versionBlockImgCell}>
                        <Image
                            src={imagePaths.get("node")}
                            alt={"Image of node logo"}
                            height={49}
                            width={168}
                        />
                    </div>
                    <div className={styles.versionBlockCell}>
                        <p>{`v${window.versions.node()}`}</p>
                    </div>
                    <div className={styles.versionBlockImgCell}>
                        <Image
                            src={imagePaths.get("chromium")}
                            alt={"Image of chromium logo"}
                            height={64}
                            width={64}
                        />
                    </div>
                    <div className={styles.versionBlockCell}>
                        <p>{`v${window.versions.chrome()}`}</p>
                    </div>
                    <div className={styles.versionBlockImgCell}>
                        <Image
                            src={imagePaths.get("electron")}
                            alt={"Image of electron logo"}
                            height={75}
                            width={75}
                        />
                    </div>
                    <div className={styles.versionBlockCell}>
                        <p>{`v${window.versions.electron()}`}</p>
                    </div>
                </div>
            ) : (
                <p>Versions not available yet...</p>
            )}
        </div>
    );
};
export default DisplayVersions;
