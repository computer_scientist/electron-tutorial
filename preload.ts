import { contextBridge } from "electron";

/**
 * Represents the versions of Node, Chrome, and Electron.
 * @interface
 * @property {Function} node - Returns the version of Node.
 * @property {Function} chrome - Returns the version of Chrome.
 * @property {Function} electron - Returns the version of Electron.
 */
contextBridge.exposeInMainWorld("versions", {
    node: () => process.versions.node,
    chrome: () => process.versions.chrome,
    electron: () => process.versions.electron,
});
