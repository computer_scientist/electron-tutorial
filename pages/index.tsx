import HtmlHead from "../components/HtmlHead";
import MainHeaderImage from "../components/MainHeaderImage";
import ContentHeader from "../components/ContentHeader";
import dynamic from "next/dynamic";
import React from "react";

// This is a dynamic import, which means that it will be loaded only when it is
// needed. This is useful for components that are not used on every page.
const DisplayVersions: React.ComponentType = dynamic(
    () => import("../components/DisplayVersions"),
    {
        ssr: false,
    }
);

/**
 * This is the root component of the Next.js app.
 * It is used to initialize pages.
 * @returns {React.JSX.Element}
 * @constructor
 */
const MainApp: React.FC = (): React.JSX.Element => {
    return (
        <>
            <HtmlHead />
            <MainHeaderImage />
            <ContentHeader />
            <DisplayVersions />
        </>
    );
};
export default MainApp;
