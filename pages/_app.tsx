import "../styles/globals.scss";
import { AppProps } from "next/app";
import React from "react";

/**
 * This is the root component of the Next.js app.
 * It is used to initialize pages.
 * @param {AppProps} props
 * @returns {React.JSX.Element}
 * @constructor
 */
const App: React.FC<AppProps> = ({
    Component,
    pageProps,
}: AppProps): React.JSX.Element => {
    return <Component {...pageProps} />;
};
export default App;
