import path from "path";
import url from "url";
import isDev from "electron-is-dev";
import { app, BrowserWindow } from "electron";

/**
 * Represents the options for creating a window.
 * @interface
 */
interface WindowOptions {
    width: number;
    height: number;
    webPreferences: {
        devTools: boolean;
        nodeIntegration: boolean;
        contextIsolation: true;
        sandbox: true;
        preload: any;
    };
}

/**
 * Function to create a window using Electron's BrowserWindow class
 *
 * @function createWindow
 * @returns {void}
 */
const createWindow = (): void => {
    const options: WindowOptions = {
        width: 800,
        height: 600,
        webPreferences: {
            devTools: true,
            nodeIntegration: false,
            contextIsolation: true,
            sandbox: true,
            preload: path.join(__dirname, "preload.js"),
        },
    };

    // Create the browser window.
    const win: BrowserWindow = new BrowserWindow(options);

    // Load the index.html of the app.
    let startUrl: string;

    if (isDev) {
        startUrl = "http://localhost:3000";
    } else {
        startUrl = url.format({
            pathname: path.join(__dirname, "./out/index.html"),
            protocol: "file:",
            slashes: true,
        });
    }

    // Load the start URL.
    win.loadURL(startUrl).catch((error: Error): void => {
        console.error(`Failed to load: ${error.message}`);
    });
};

// Create the window when Electron has finished initialization.
app.whenReady().then((): void => {
    createWindow();

    app.on("activate", (): void => {
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow();
        }
    });
});

// Quit when all windows are closed, except on macOS.
app.on("window-all-closed", (): void => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});
