# Electron Official Tutorial

## Description

This project was from the official tutorial from the Electron Website.
It is a simple application that uses the Next.js
framework to create a simple application that displays the current time.
The approach was a bit different from the
official tutorial as I used the Next.js framework, React Framework, Typescript, and Sass.
The reason for this is that I
wanted to use these frameworks together as a reference for future projects.

Though Next.js is not required for Electron,
it is a good framework with a lot of features that allow for easy development.
This approach is not easy to deal with as there are incompatibilities between Next.js and Electron.
The main issue is that Next.js uses webpack to bundle the application.
Another issue is the default Server Side Rendering (SSR) that Next.js uses.
This is not needed for Electron as it is a desktop application and does not need to be rendered on a server.
The solution to this is to use the Next.js framework to create a static application.
So, yes, next.js can render client-side applications too 🤩.

Another interesting approach is the use of Typescript.
Typescript is a superset of Javascript that allows for type checking similar to rust.
Next.js has a built-in Typescript support.
So, typescript on the React side is not an issue since Next.js handles it.
But on the Electron side, it is a bit tricky.
The way I handled it was to separate the tsconfig.json files that the next.js framework
uses and the tsconfig.json file that the electron uses.
So Next.js will use the default tsconfig.json file and electron will use a custom tsconfig.electron.json file.
This can be viewed in the repository.

Finally, the project uses Sass for styling.
There were a number of modifications and additions to the
tutorial that was provided by the official source.
The animation from the landing page was entered to the project.
This animation was essentially looped using react hooks.
Another modification to the logo was the addition of the logo reverse animation.
Many of these were a challenge that I gave myself to learn more about the frameworks and apply what I know.

## Installation

Initialize npm

```zsh
npm init
```

Install dev dependencies

```zsh
npm install --save-dev electron@latest next@latest react@latest react-dom@latest sass@latest @electron-forge/cli@latest
```