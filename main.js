"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const url_1 = __importDefault(require("url"));
const electron_is_dev_1 = __importDefault(require("electron-is-dev"));
const electron_1 = require("electron");
/**
 * Function to create a window using Electron's BrowserWindow class
 *
 * @function createWindow
 * @returns {void}
 */
const createWindow = () => {
    const options = {
        width: 800,
        height: 600,
        webPreferences: {
            devTools: true,
            nodeIntegration: false,
            contextIsolation: true,
            sandbox: true,
            preload: path_1.default.join(__dirname, "preload.js"),
        },
    };
    // Create the browser window.
    const win = new electron_1.BrowserWindow(options);
    // Load the index.html of the app.
    let startUrl;
    if (electron_is_dev_1.default) {
        startUrl = "http://localhost:3000";
    }
    else {
        startUrl = url_1.default.format({
            pathname: path_1.default.join(__dirname, "./out/index.html"),
            protocol: "file:",
            slashes: true,
        });
    }
    // Load the start URL.
    win.loadURL(startUrl).catch((error) => {
        console.error(`Failed to load: ${error.message}`);
    });
};
// Create the window when Electron has finished initialization.
electron_1.app.whenReady().then(() => {
    createWindow();
    electron_1.app.on("activate", () => {
        if (electron_1.BrowserWindow.getAllWindows().length === 0) {
            createWindow();
        }
    });
});
// Quit when all windows are closed, except on macOS.
electron_1.app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        electron_1.app.quit();
    }
});
